import React from 'react';
import './App.css';
import {BondCardContainer} from './modules/BondCard/BondCardContainer';

function App() {
    return (
        <div className="app">
            <BondCardContainer id="US67021BAE92" />
        </div>
    );
}

export default App;
