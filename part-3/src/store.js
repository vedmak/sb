import {createStore, applyMiddleware, combineReducers} from 'redux';
import thunk from 'redux-thunk';
import {reducer as bondCardReducer, rootKey as rootKeyBondCard} from './modules/BondCard/reducer';

const rootReducer = combineReducers({
    [rootKeyBondCard]: bondCardReducer,
});

export const store = createStore(rootReducer, applyMiddleware(thunk));
