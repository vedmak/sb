import React from 'react';
import PropTypes from 'prop-types';
import './style.css';

export function ButtonRadioGroup(props) {
    return <div className="button-radio-group">
        {props.buttons.map(button => {
            return <button
                key={button.value}
                className={props.value === button.value ? 'active' : ''}
                onClick={() => props.onSelect(button.value)}
            >
                {button.label}
            </button>
        })}
    </div>;
}

ButtonRadioGroup.propTypes = {
    buttons: PropTypes.arrayOf(PropTypes.shape({value: PropTypes.string, label: PropTypes.string})).isRequired,
    onSelect: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired,
};
