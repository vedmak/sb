function randomInteger(min, max) {
    return Math.floor(min + Math.random() * (max + 1 - min));
}

export function generateBondData(id) {
    return {
        id,
        label: 'NII CAPITAL 7.625 21',
        currency: 'USD',
        description: 'NII CAPITAL CORP, Telecommunications, NR, till 01.03.2016',

        stats: ['week', 'month', 'quarter', 'year', 'max'].reduce(
            (periods, period) => {
                periods[period] = ['yield', 'spread', 'price'].reduce(
                    (types, type) => {
                        types[type] = [...Array(randomInteger(5, 10)).keys()].map(I => ({
                            date: I + 1,
                            value: randomInteger(50, 100)
                        }));

                        return types;
                    },

                    {}
                );

                return periods;
            },

            {}
        )
    };
}

export const api = {
    getBond: (id) => {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve(generateBondData(id));
            }, 1500)
        })
    }
}