import React from 'react';
import PropTypes from 'prop-types';
import {BondCardComponent} from '../BondCardComponent';
import {useDispatch, useSelector} from 'react-redux';
import {bondRequest} from '../actions';
import {getData, getIsLoading, getIsError} from '../selectors';
import {Loader} from '../../../common/components/Loader';
import {ErrorMessage} from '../../../common/components/ErrorMessage';

export function BondCardContainer(props) {
    const dispatch = useDispatch();
    const bond = useSelector(getData(props.id));
    const isLoading = useSelector(getIsLoading(props.id));
    const isError = useSelector(getIsError(props.id));

    React.useEffect(
        () => {
            dispatch(bondRequest(props.id))
        },

        [dispatch, props.id]
    );

    if (isLoading) {
        return <Loader />;
    }

    if (isError) {
        return <ErrorMessage />;
    }

    if (bond) {
        return <BondCardComponent
            id={bond.id}
            label={bond.label}
            currency={bond.currency}
            description={bond.description}
            stats={bond.stats}
        />
    }

    return null;
};

BondCardContainer.propTypes = {
    id: PropTypes.string.isRequired,
};
