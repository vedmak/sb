import {rootKey} from './reducer';

const getBondState = (state, id) => {
    return state[rootKey][id] || {};
}

export const getIsLoading = id => state => {
    const bondState = getBondState(state, id);
    return bondState.isLoading || false;
}

export const getIsError = id => state => {
    const bondState = getBondState(state, id);
    return bondState.isError || false;
}

export const getData = id => state => {
    const bondState = getBondState(state, id);
    return bondState.data || null;
}