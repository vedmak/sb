import React from 'react';
import PropTypes from 'prop-types';
import {LineChart, Line, CartesianGrid, XAxis, YAxis} from 'recharts';
import './style.css';
import {ButtonRadioGroup} from '../../../common/components/ButtonRadioGroup';

export function BondCardComponent(props) {
    const [periodGraph, setPeriodGraph] = React.useState('week');
    const [byTypeGraph, setByTypeGraph] = React.useState('price');

    const periodButtons = [
        {label: 'Week', value: 'week'},
        {label: 'Month', value: 'month'},
        {label: 'Quarter', value: 'quarter'},
        {label: 'Year', value: 'year'},
        {label: 'Max', value: 'max'},
    ];

    const typeOptions = [
        {label: 'Yield', value: 'yield'},
        {label: 'Spread', value: 'spread'},
        {label: 'Price', value: 'price'},
    ];

    const graphData = React.useMemo(
        () => {
            return props.stats[periodGraph][byTypeGraph];
        },

        [props.stats, periodGraph, byTypeGraph]
    );

    return <div className="bond-card">
        <div className="bond-card--header">
            <h2>{props.label}</h2>
            <span>{props.currency}</span>
        </div>

        <div className="bond-card--description">
            <div>{props.id}</div>
            <div>{props.description}</div>
        </div>

        <div className="bond-card--graph">
            <div className="bond-card--graph-buttons">
                <ButtonRadioGroup
                    buttons={periodButtons}
                    value={periodGraph}
                    onSelect={(value) => {setPeriodGraph(value)}}
                />
            </div>

            <div className="bond-card--graph-canvas">
                <LineChart width={400} height={250} data={graphData}>
                    <Line type="monotone" dataKey="value" stroke="#8884d8" />
                    <CartesianGrid stroke="#ccc" />
                    <XAxis dataKey="date" />
                    <YAxis />
                </LineChart>

                <select value={byTypeGraph} onChange={(event) => {setByTypeGraph(event.target.value)}}>
                    {typeOptions.map(option => {
                        return <option
                            key={option.value}
                            value={option.value}
                        >
                            {option.label}
                        </option>
                    })}
                </select>
            </div>
        </div>
    </div>
}

BondCardComponent.propTypes = {
    id: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    currency: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    stats: PropTypes.object.isRequired,
}
