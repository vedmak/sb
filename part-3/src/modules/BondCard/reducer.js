import {handleActions} from 'redux-actions';
import {_bondRequest, _bondSuccess, _bondFailure} from './actions';

export const reducer = handleActions(
    {
        [_bondRequest]: (state, {payload: {bondId}}) => {
            const currentState = state[bondId] || {};

            return {
                ...state,
                [bondId]: {
                    ...currentState,
                    isLoading: true,
                    isError: false,
                }
            }
        },

        [_bondFailure]: (state, {payload: {bondId}}) => {
            const currentState = state[bondId] || {};

            return {
                ...state,
                [bondId]: {
                    ...currentState,
                    isLoading: false,
                    isError: true,
                }
            }
        },

        [_bondSuccess]: (state, {payload: {bondId, bondData}}) => {
            const currentState = state[bondId] || {};

            return {
                ...state,
                [bondId]: {
                    ...currentState,
                    isLoading: false,
                    data: bondData,
                }
            }
        }
    },

    {}
);

export const rootKey = 'bondCards';
