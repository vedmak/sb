import {createAction} from 'redux-actions';
import {api} from '../../common/api';

export const _bondRequest = createAction('BOND_REQUEST');
export const _bondSuccess = createAction('BOND_SUCCESS');
export const _bondFailure = createAction('BOND_FAILURE');

export const bondRequest = (bondId) => async (dispatch) => {
    dispatch(_bondRequest({bondId}));

    try {
        const bondData = await api.getBond(bondId);
        dispatch(_bondSuccess({bondId, bondData}));
    } catch {
        dispatch(_bondFailure({bondId}));
    }
}
