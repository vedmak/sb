import React from 'react';
import {Provider} from 'react-redux';
import {BondCardContainer} from '../BondCardContainer/index';
import {render} from '@testing-library/react';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import {rootKey} from '../reducer';
import {_bondRequest} from '../actions';
import {generateBondData} from '../../../common/api';

const mockStore = configureStore([thunk]);
const bondId = 'US67021BAE92';

function renderComponent(store) {
    return render(<Provider store={store}>
        <BondCardContainer id="US67021BAE92" />
    </Provider>)
}

describe('BondCardContainer', () => {
    test('must dispatch request action', () => {
        const store = mockStore({[rootKey]: {}});
        renderComponent(store);

        expect(store.getActions()).toContainEqual(
            {type: _bondRequest.toString(), payload: expect.anything()}
        );
    });

    test('render loader', () => {
        const store = mockStore({[rootKey]: {[bondId]: {isLoading: true}}});
        const {getByText} = renderComponent(store);
        expect(getByText(/loading/i)).toBeInTheDocument();
    });

    test('render error', () => {
        const store = mockStore({[rootKey]: {[bondId]: {isError: true}}});
        const {getByText} = renderComponent(store);
        expect(getByText(/wrong/i)).toBeInTheDocument();
    });

    test('render bond info', () => {
        const data = generateBondData(bondId);
        const store = mockStore({[rootKey]: {[bondId]: {data}}});
        const {getByText} = renderComponent(store);
        expect(getByText(data.label)).toBeInTheDocument();
        expect(getByText(data.currency)).toBeInTheDocument();
        expect(getByText(data.description)).toBeInTheDocument();
        expect(getByText(data.id)).toBeInTheDocument();
    });
});

