let cacheStorage = {};

function generateKey(date, isin) {
    return JSON.stringify([date || 'empty-date', isin]);
}

export const cache = {
    getData: (date, isin) => {
        return cacheStorage[generateKey(date, isin)];
    },

    setData: (date, isin, data) => {
        cacheStorage[generateKey(date, isin)] = data;
    },

    clearCache: () => {
        cacheStorage = {};
    },
};