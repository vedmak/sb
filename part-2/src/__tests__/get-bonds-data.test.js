import {getBondsData} from '../get-bonds-data';
import {cache} from '../bonds-cache';
import {http} from '../http';

jest.mock('../http', () => {
    return {
        __esModule: true,

        http: {
            post: jest.fn(),
        },
    };
});

describe('getBondsData', () => {
    beforeEach(() => {
        cache.clearCache();
    });

    test('make right server request', async () => {
        let date = '20180101';
        const isins = ['1', '2'];

        http.post.mockReturnValueOnce(Promise.resolve([
            {isin: '1', data: 'data'},
            {isin: '2', data: 'data'},
        ]));

        await getBondsData({date, isins});
        expect(http.post).toBeCalledWith({url: expect.anything(), body: isins});

        http.post.mockReturnValueOnce(Promise.resolve([
            {isin: '3', data: 'data'},
        ]));

        isins.push('3');

        await getBondsData({date, isins});
        expect(http.post).toBeCalledWith({url: expect.anything(), body: ['3']});

        http.post.mockReturnValueOnce(Promise.resolve([
            {isin: '1', data: 'data'},
            {isin: '2', data: 'data'},
            {isin: '3', data: 'data'},
        ]));

        date = '20180102';
        await getBondsData({date, isins});
        expect(http.post).toBeCalledWith({url: expect.anything(), body: ['1', '2', '3']});
    });

    test('right order after get data from cache', async () => {
        const date = '20180101';
        let isins = ['3'];

        http.post.mockReturnValueOnce(Promise.resolve([
            {isin: '3', data: 'data'},
        ]));

        await getBondsData({date, isins});

        isins = ['1', '2', '3', '4', '5'];

        http.post.mockReturnValueOnce(Promise.resolve([
            {isin: '1', data: 'data'},
            {isin: '2', data: 'data'},
            {isin: '4', data: 'data'},
            {isin: '5', data: 'data'},
        ]));

        const response = await getBondsData({date, isins});

        isins.forEach((I, index) => {
            expect(I).toBe(response[index].isin);
        })
    });
});
