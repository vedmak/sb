import {http} from './http';
import {cache} from './bonds-cache';

export const getBondsData = async ({date, isins}) => {
    const resultFromCache = isins.reduce(
        (result, I) => {
            result[I] = cache.getData(date, I) || false;
            return result;
        },

        {}
    );

    const isinsWithoutCachedData = isins.filter(I => !resultFromCache[I]);

    const resultFromServer = await http.post({
        url: `/bonds/${date}`,
        body: isinsWithoutCachedData,
    });

    isinsWithoutCachedData.forEach((I, index) => cache.setData(date, I, resultFromServer[index]));

    return isins.map(I => {
        return resultFromCache[I] || resultFromServer.shift()
    });
};