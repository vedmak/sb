export const http = {
    post: ({url, body}) => {
        if (!/\/bonds\//.test(url)) return Promise.resolve();

        return Promise.resolve(body.map(I => ({
            isin: I,
            data: 'data'
        })));
    }
}