function preparePercentOutput(percent) {
    return percent.toFixed(3);
}

export function calculatePercents(arr) {
    arr.forEach((I, index) => {
        if (typeof I !== 'number') {
            throw Error(`element number ${index + 1} is not number`)
        } else if (isNaN(I)) {
            throw Error(`element number ${index + 1} is NaN`)
        } else if (I < 0) {
            throw Error(`element number ${index + 1} is less than zero`)
        }
    });

    if (arr.length === 1) {
        return [preparePercentOutput(100)];
    }

    const onePercent = arr.reduce(
        (sum, I) => sum + I,
        0
    ) / 100;

    return arr.map(I => preparePercentOutput(I / onePercent));
}
