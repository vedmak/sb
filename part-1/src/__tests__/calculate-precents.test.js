import {calculatePercents} from '../calculate-percents';

test('calculate right percents', () => {
    expect(calculatePercents([3,3,3])).toEqual(['33.333', '33.333', '33.333']);
    expect(calculatePercents([5,5])).toEqual(['50.000', '50.000']);
    expect(calculatePercents([1])).toEqual(['100.000']);
    expect(calculatePercents([5, 0])).toEqual(['100.000', '0.000']);
});

test.each([
    [[1, NaN]],
    [[1, -1]],
    [[1, 'string']],
])('wrong arguments %p must throw error', (arr) => {
    expect(() => calculatePercents(arr)).toThrow();
});

describe('sum', () => {
    const maximumDifference = 0.1;

    function randomInteger(min, max) {
        return Math.floor(min + Math.random() * (max + 1 - min));
    }

    function generateRandomData(isInteger) {
        return [...Array(1000).keys()].map(() => {
            return [[...Array(1000).keys()].map(() => isInteger ? randomInteger(1, 1000) : Math.random())]
        })
    }

    function getArrSum(arr) {
        return arr.reduce(
            (sum, I) => sum + parseFloat(I),
            0
        );
    }

    function getDifference(percents) {
        return parseFloat((100 - getArrSum(percents)).toFixed(3));
    }

    test.each(
        generateRandomData(false)
    )('float - sum percents of %p must be 100 (with permissible error)', (arr) => {
        expect(getDifference(calculatePercents(arr))).toBeLessThanOrEqual(maximumDifference);
    });

    test.each(
        generateRandomData(true)
    )('integer - sum percents of %p must be 100 (with permissible error)', (arr) => {
        expect(getDifference(calculatePercents(arr))).toBeLessThanOrEqual(maximumDifference);
    });
})